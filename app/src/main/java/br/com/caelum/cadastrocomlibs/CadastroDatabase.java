package br.com.caelum.cadastrocomlibs;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import br.com.caelum.cadastrocomlibs.model.Aluno;

@Database(entities = {Aluno.class}, version = 1)
public abstract class CadastroDatabase extends RoomDatabase {

    public abstract AlunoDao getAlunoDao();
}
