package br.com.caelum.cadastrocomlibs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.caelum.cadastrocomlibs.adapter.MeuAdapterDeAluno;
import br.com.caelum.cadastrocomlibs.model.Aluno;

public class ListaAlunosFragment extends Fragment {


    private RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_lista_alunos, container, false);


        recyclerView = view.findViewById(R.id.recycler_alunos);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        carregalista();
    }

    private void carregalista() {

        AlunoDao dao = new GeradorDeBancoDeDados().gera(getContext());
        List<Aluno> alunos = dao.busca();

        recyclerView.setAdapter(new MeuAdapterDeAluno(alunos));
    }


}
