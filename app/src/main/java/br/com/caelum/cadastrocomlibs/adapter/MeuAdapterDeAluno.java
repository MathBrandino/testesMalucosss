package br.com.caelum.cadastrocomlibs.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.com.caelum.cadastrocomlibs.R;
import br.com.caelum.cadastrocomlibs.model.Aluno;

public class MeuAdapterDeAluno extends RecyclerView.Adapter {
    private List<Aluno> alunos;

    public MeuAdapterDeAluno(List<Aluno> alunos) {
        this.alunos = alunos;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_aluno, parent, false);
        MeuViewHolder meuViewHolder = new MeuViewHolder(view);
        return meuViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        MeuViewHolder viewHolder = (MeuViewHolder) holder;

        Aluno aluno = alunos.get(position);

        viewHolder.texto.setText(aluno.getNome());
    }

    @Override
    public int getItemCount() {
        return alunos.size();
    }


    class MeuViewHolder extends RecyclerView.ViewHolder {
        TextView texto;

        public MeuViewHolder(View itemView) {
            super(itemView);
            texto = itemView.findViewById(R.id.item);
        }
    }
}
