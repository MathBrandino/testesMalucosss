package br.com.caelum.cadastrocomlibs;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import br.com.caelum.cadastrocomlibs.model.Aluno;

@Dao
public interface AlunoDao {

    @Insert
    void insere(Aluno aluno);

    @Query("select * from Aluno order by nome")
    List<Aluno> busca();
}
