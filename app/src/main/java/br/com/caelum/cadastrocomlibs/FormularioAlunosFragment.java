package br.com.caelum.cadastrocomlibs;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import br.com.caelum.cadastrocomlibs.model.Aluno;

public class FormularioAlunosFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lista_provas, container, false);

        final EditText campoNome = view.findViewById(R.id.formulario_nome);
        final TextInputLayout campoTil = view.findViewById(R.id.til_nome);

        Button btn = view.findViewById(R.id.salvar);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (campoNome.getText().toString().isEmpty()) {
                    campoTil.setError("Vazio não filhão");
                    return;
                }

                Aluno aluno = new Aluno();

                aluno.setNome(campoNome.getText().toString());

                AlunoDao dao = new GeradorDeBancoDeDados().gera(getContext());
                dao.insere(aluno);

                startActivity(new Intent(getContext(), MainActivity.class));

            }
        });

        return view;

    }
}
