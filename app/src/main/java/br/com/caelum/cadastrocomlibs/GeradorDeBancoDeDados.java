package br.com.caelum.cadastrocomlibs;

import android.arch.persistence.room.Room;
import android.content.Context;

public class GeradorDeBancoDeDados {

    public AlunoDao gera(Context contexto) {

        CadastroDatabase db = Room.databaseBuilder(contexto,
                CadastroDatabase.class,
                "CadastroDB")
                .allowMainThreadQueries()
                .build();

        return db.getAlunoDao();

    }

}
